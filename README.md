Perfect Cue to OSC

This patch will convert Perfect Cue clicker to OSC commands. 

This is very useful for corporate / conference shows where the presenter uses presentation clicker to control their presentation.

The default settings/commands work with d3 disguise media server. Just make sure osc device is set up inside d3. 

To use with other media server/osc devices, just rename the osc commands, and the network settings.

Developed on TouchDesigner version 99.2018.27840. Mac & Windows. 